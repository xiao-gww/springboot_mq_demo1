package com.sz.demo.test;


import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

@Component
public class Que {

    @Value("${queue}")
    private String queue;

    @Bean
    public Queue getQueue(){
        return new ActiveMQQueue(queue);
    }
}
